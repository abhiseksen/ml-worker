FROM python:3.7.7

RUN echo running apt-get update...
RUN apt-get update

WORKDIR /run

COPY requirements.txt /run
RUN pip install -r requirements.txt
COPY . /run
CMD bash
