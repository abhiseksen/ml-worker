#!/usr/bin/env bash

# bash functions for deployment task

function wait_for_dag_deployment_finish {
  n=0
  until [[ $n -ge $4 ]]
  do
    status=0
    gcloud composer environments run "${1}" --location "${2}" list_dags \
    2>&1 | grep "${3}" && break
    status=$?
    n=$(($n+1))
    sleep "${5}"
  done
  exit $status
}

wait_for_dag_deployment_finish()
