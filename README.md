# README #

This repo contains ML components and relevant APIs.

### How do I get set up? ###

* Summary of set up
For local development, please use
```
docker-compose build
```

If build is successful, you can start the service with:
```
docker-compose up -d
```

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
