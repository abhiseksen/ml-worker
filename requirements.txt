python-dotenv==0.14.0
psycopg2-binary==2.8.6
pandas==1.1.1
Django==3.1.1
gunicorn==19.9.0
djangorestframework==3.11.1
