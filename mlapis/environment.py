import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
DEVLOPMENT_ENV = os.environ.get('DEVELOPMENT_ENV', 'dev')
DJANGO_DATABASE_NAME = os.environ.get('DJANGO_DATABASE_NAME')
DJANGO_DATABASE_HOST = os.environ.get('DJANGO_DATABASE_HOST')
DJANGO_DATABASE_USER = os.environ.get('DJANGO_DATABASE_USER')
DJANGO_DATABASE_PASS = os.environ.get('DJANGO_DATABASE_PASS')
